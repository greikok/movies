import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private loadingController: LoadingController) { }

  async loading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 3000
    });
   return loading.present();
  }
}
