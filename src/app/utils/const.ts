export const DEFAULTIMAGE = '../../../assets/default.png';
export const ERRORIMAGE = '../../../assets/notfound.jpg';
export const PAGENOTFOUND = '../../assets/pagenotfound.jpg';
export const toastSucces = 'success';
export const toastError = 'danger';
export const toastNotice = 'dark';

