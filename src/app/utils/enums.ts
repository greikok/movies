export enum ToastMessages {
  saveChanged = 'Tus cambios han sido guardados.',
  createMovie = 'Tu película ha sido creada',
  deletedChanged = 'Tu película ha sido borrada',
  errorSavingChanges = 'Hubo un error guardando los cambios.',
  errorGettingMovies = 'No se pueden obtener las películas.',
  errorDeletingMovies = 'Hubo un error al borrar la película',

};
