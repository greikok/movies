import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.page.html',
  styleUrls: ['./actor.page.scss'],
})
export class ActorPage implements OnInit {
  nameHeader: string;
  backHome = true;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.getTranslate();
  }

  getTranslate() {
    this.translate.stream('ACTORS').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }
}
