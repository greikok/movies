import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Actor } from '@interfaces/actor';
import { ToastService } from '@services/toast.service';
import { toastError } from '@utils/const';
import { ToastMessages } from '@utils/enums';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActorService {
  apiUrl = environment.apiUrl;
  public actors: Actor[];

  constructor(private http: HttpClient, private toastService: ToastService) { }

  getActors(): Observable<Actor[]> {
    if (this.actors) {
      return of(this.actors);
    }
    return this.http.get<Actor[]>(`${this.apiUrl}/actors`)
      .pipe(
        tap(resp =>{
          this.actors = resp;
       }),
        catchError(error => {
          this.toastService.toast(ToastMessages.errorGettingMovies, toastError);
          throw new Error(error);
        }
        ));
  }

  getNameOfActors(actorsMovie, actors) {
    const actorsList = [];
    actors.forEach((actor) => {
      const fullName = actor.first_name + ' ' + actor.last_name;
      actorsMovie.forEach(actorMovieId => {
        if (actorMovieId === actor.id) {
          actorsList.push(actor);
        }
      });
    });
    return actorsList;
  }
}
