import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Movie } from '@interfaces/movie';
import { TranslateService } from '@ngx-translate/core';
import { MovieService } from './services/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})
export class MoviesPage implements OnInit {

  movies: Movie[];
  loadingMovie = false;
  backHome = false;
  nameHeader: string;

  constructor(private movieService: MovieService, private router: Router,
    private translate: TranslateService) { }

  ngOnInit() {
    this.getMovies();
    this.getTranslate();
  }

  getMovies() {
    this.movieService.getMovies().subscribe((allMovies: Movie[]) => {
      this.movies = allMovies;
      this.loadingMovie = true;
    }, (error => {
    }));
  }

  getTranslate() {
    this.translate.stream('MOVIES').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }

}
