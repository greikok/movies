import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { Movie } from '@interfaces/movie';
import { ToastService } from '@services/toast.service';
import { ToastMessages } from '@utils/enums';
import { toastError } from '@utils/const';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  apiUrl = environment.apiUrl;
  movies: Movie[];

  constructor(private http: HttpClient, private toastService: ToastService) { }

  getMovies(): Observable<Movie[]> {
    if (this.movies) {
      return of(this.movies);
    }
    return this.getAllMovies();
  }

  getAllMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.apiUrl}/movies`)
      .pipe(
        tap(resp =>{
           this.movies = resp;
        }),
        catchError(error => {
          this.toastService.toast(ToastMessages.errorGettingMovies, toastError);
          throw new Error(error);
        }
        ));
  }

  createMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(`${this.apiUrl}/movies`, movie)
      .pipe(
        catchError(error => {
          this.toastService.toast(ToastMessages.errorGettingMovies, toastError);
          throw new Error(error);
        }
        ));
  }

  getMovie(id: number): Observable<Movie> {
    return this.http.get<Movie>(`${this.apiUrl}/movies/${id}`)
      .pipe(
        catchError(error => {
          this.toastService.toast(ToastMessages.errorGettingMovies, toastError);
          throw new Error(error);
        }
        ));
  }

  deleteMovie(id: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/movies/${id}`)
      .pipe(
        catchError(error => {
          this.toastService.toast(ToastMessages.errorDeletingMovies, toastError);
          throw new Error(error);
        }
        ));
  }

  updateMovie(id: number, movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(`${this.apiUrl}/movies/${id}`, movie)
      .pipe(
        catchError(error => {
          this.toastService.toast(ToastMessages.errorSavingChanges, toastError);
          throw new Error(error);
        }
        ));
  }
}
