import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateMovieComponent } from './create-movie/create-movie.component';
import { DetailsMovieComponent } from './details-movie/details-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';

import { MoviesPage } from './movies.page';

const routes: Routes = [
  {
    path: '',
    component: MoviesPage,
    pathMatch: 'full',
  },
  {
    path: 'create',
    component: CreateMovieComponent
  },
  {
    path: 'details/:id',
    component: DetailsMovieComponent
  },
  {
    path: 'edit/:id',
    component: EditMovieComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesPageRoutingModule { }
