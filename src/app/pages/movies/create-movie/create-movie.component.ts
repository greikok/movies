import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActorService } from 'app/pages/actor/services/actor.service';
import { Actor } from '@interfaces/actor';
import { StudieService } from 'app/pages/studies/services/studie.service';
import { Studie } from '@interfaces/studie.interface';
import { MovieService } from '../services/movie.service';
import { ToastService } from '@services/toast.service';
import { ToastMessages } from '@utils/enums';
import { toastSucces } from '@utils/const';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-create-movie',
  templateUrl: './create-movie.component.html',
  styleUrls: ['./create-movie.component.scss'],
})

export class CreateMovieComponent implements OnInit {
  movieForm: FormGroup;
  actors: Actor[];
  studies: Studie[];
  actorsForm = [];
  studiesForm = [];
  genreFiel = '';
  genreForm = [];
  nameHeader: string;
  backHome = true;

  constructor(private route: Router,
    private fb: FormBuilder,
    private actorService: ActorService,
    private studieService: StudieService,
    private movieService: MovieService,
    private toastMessage: ToastService,
    private translate: TranslateService) { }

  ngOnInit() {
    this.initFormMovie();
    this.getActors();
    this.getStudies();
    this.getTranslate();
  }

  initFormMovie() {
    this.movieForm = this.fb.group({
      title: ['',Validators.required],
      poster: ['',Validators.required],
      genre: this.fb.array([]),
      year: ['',Validators.pattern['[0-9]']],
      duration: ['',Validators.pattern['[0-9]']],
      imdbRating: [''],
      actors: this.fb.array([]),
      companie: [''],
    });
  }

  get movieActors() {
    return this.movieForm.get('actors') as FormArray;
  }

  get movieGenres() {
    return this.movieForm.get('genre') as FormArray;
  }

  addMovieActors(actor: number) {
    this.movieActors.push(this.fb.control(actor));
  }

  removeMovieActors(index: number) {
    this.movieActors.removeAt(index);
  }

  addMovieGenres(genre: string) {
    this.movieGenres.push(this.fb.control(genre));
  }

  removeMovieGenres(index: number) {
    this.movieGenres.removeAt(index);
  }

  onSave() {
    this.movieService.createMovie(this.movieForm.value).subscribe((movie) => {
      this.movieService.movies.push(movie);
      this.toastMessage.toast(ToastMessages.createMovie, toastSucces);
      this.movieForm.reset();
      this.actorsForm = [];
      this.genreForm = [];
      setTimeout(() => {
        this.route.navigate(['']);
      }, 3000);
    });
  }

  addGenreGroup() {
    return this.fb.group({
      genre: ['']
    });
  }

  addActorGroup() {
    return this.fb.group({
      actor: ['']
    });
  }

  getActors() {
    this.actorService.getActors().subscribe((allActors: Actor[]) => {
      this.actors = allActors;
    }, (error => {
    }));
  }

  getStudies() {
    this.studieService.getStudies().subscribe((allStudies: Studie[]) => {
      this.studies = allStudies;
    }, (error => {
    }));
  }

  onChangeActorSelected(actorSelected) {
    actorSelected = actorSelected.detail.value;
    this.addMovieActors(actorSelected.id);

    this.actors.forEach((actor, index) => {
      if (actor.id === actorSelected.id) {
        this.actorsForm.push(actorSelected);
      }
    });

  }

  addGenre() {
    this.addMovieGenres(this.genreFiel);
    this.genreForm.push(this.genreFiel);
    this.genreFiel = '';
  }

  removeGenreChip(genreSelected: string) {
    this.genreForm.forEach((genre, index) => {
      if (genre === genreSelected) {
        this.genreForm.splice(index, 1);
        this.removeMovieGenres(index);
      }
    });
  }

  removeActorChip(actorRemove) {
    this.actorsForm.forEach((actor, index) => {
      if (actor.id === actorRemove.id) {
        this.actorsForm.splice(index, 1);
        this.removeMovieActors(index);
        this.actors.push(actorRemove);
      }
    });
  }

  getTranslate() {
    this.translate.stream('CREATEMOVIE').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }

}
