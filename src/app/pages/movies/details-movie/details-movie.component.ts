import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actor } from '@interfaces/actor';
import { Movie } from '@interfaces/movie';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '@services/loading.service';
import { ToastService } from '@services/toast.service';
import { toastSucces } from '@utils/const';
import { ToastMessages } from '@utils/enums';
import { ActorService } from 'app/pages/actor/services/actor.service';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-details-movie',
  templateUrl: './details-movie.component.html',
  styleUrls: ['./details-movie.component.scss'],
})
export class DetailsMovieComponent implements OnInit {

  movie: Movie;
  actorsList = [];
  showDetails = false;
  canDeleteMovie = false;
  nameHeader: string;
  backHome = true;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private movieService: MovieService,
    private actorService: ActorService,
    private loadingService: LoadingService,
    private toastMessage: ToastService,
    private translate: TranslateService) {
  }

  ngOnInit() {
   this.loadingService.loading();
    this.getMovie();
    this.getTranslate();
  }

  getActors(actorsMovie) {
    this.actorService.getActors().subscribe((allActors) => {
      allActors.forEach((actor) => {
        const fullName = actor.first_name + actor.last_name;
        actorsMovie.forEach(actorMovieId => {
          if (actorMovieId === actor.id) {
            this.actorsList.push(fullName);
          }
        });
      });
      this.showDetails = true;
    });
  }

  getMovie() {
    this.movieService.getMovie(this.route.snapshot.params.id).subscribe(
      (movieDetail) => {
        this.movie = movieDetail;
        this.getActors(this.movie.actors);
      });
  }

  deleteMovie(id: number) {
    this.canDeleteMovie = true;

    this.movieService.deleteMovie(id).subscribe(()=>{
      this.movieService.movies.forEach((movie,index)=>{
        if(movie.id === id) {
          return this.movieService.movies.splice(index, 1);
        }
      });
      this.toastMessage.toast(ToastMessages.deletedChanged, toastSucces);
      setTimeout(() => {
        this.router.navigate(['']);
      }, 3000);
    },(error =>{
      this.canDeleteMovie = false;
    }));

  }

  editMovie(id: number) {
    this.router.navigate([`/movies/edit/${id}`]);
  }

  getTranslate() {
    this.translate.stream('DETAILMOVIE').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }
}
