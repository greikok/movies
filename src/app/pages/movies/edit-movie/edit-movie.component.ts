import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actor } from '@interfaces/actor';
import { Movie } from '@interfaces/movie';
import { Studie } from '@interfaces/studie.interface';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '@services/toast.service';
import { toastSucces } from '@utils/const';
import { ToastMessages } from '@utils/enums';
import { ActorService } from 'app/pages/actor/services/actor.service';
import { StudieService } from 'app/pages/studies/services/studie.service';
import { MovieService } from '../services/movie.service';


@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.scss'],
})
export class EditMovieComponent implements OnInit {
  movieForm: FormGroup;
  actors: Actor[];
  studies: Studie[];
  movie: Movie;
  actorsForm = [];
  studiesForm = [];
  genreFiel = '';
  genreForm = [];
  nameHeader: string;
  backHome = true;

  constructor(private route: Router,
    private fb: FormBuilder,
    private actorService: ActorService,
    private studieService: StudieService,
    private movieService: MovieService,
    private toastMessage: ToastService,
    private router: ActivatedRoute,
    private translate: TranslateService) { }


  ngOnInit() {
    this.getActors();
    this.getTranslate();
  }

  getMovie() {
    this.movieService.getMovie(this.router.snapshot.params.id).subscribe(
      (movieDetail) => {
        this.movie = movieDetail;
       this.initFormMovie(this.movie);
      });
  }

  initFormMovie(movie: Movie) {
    this.movieForm = this.fb.group({
      title: [movie.title],
      poster: [movie.poster],
      genre: this.fb.array([]),
      year: [movie.year],
      duration: [movie.duration],
      imdbRating: [movie.imdbRating],
      actors: this.fb.array([]),
      companie: [movie.companie],
    });
    this.setActorsForm(movie.actors);
    this.setGenreForm(movie.genre);
  }

  get movieActors() {
    return this.movieForm.get('actors') as FormArray;
  }

  get movieGenres() {
    return this.movieForm.get('genre') as FormArray;
  }

  addMovieActors(actor: number) {
    this.movieActors.push(this.fb.control(actor));
  }

  removeMovieActors(index: number) {
    this.movieActors.removeAt(index);
  }

  addMovieGenres(genre: string) {
    this.movieGenres.push(this.fb.control(genre));
  }

  removeMovieGenres(index: number) {
    this.movieGenres.removeAt(index);
  }

  onSave() {
    this.movieService.updateMovie(this.movie.id,this.movieForm.value).subscribe((movie) => {
      this.movieService.movies.forEach((movieSearch, index) =>{
        if(movieSearch.id === this.movie.id) {
          this.movieService.movies.splice(index, 1, movie);
        }
      });
      this.toastMessage.toast(ToastMessages.saveChanged, toastSucces);
      setTimeout(() => {
        this.route.navigate(['']);
      }, 3000);
    });
  }

  addGenreGroup() {
    return this.fb.group({
      genre: ['']
    });
  }

  addActorGroup() {
    return this.fb.group({
      actor: ['']
    });
  }

  getActors() {
    this.actorService.getActors().subscribe((allActors: Actor[]) => {
      this.actors = allActors;
      this.getStudies();
    }, (error => {
    }));
  }

  getStudies() {
    this.studieService.getStudies().subscribe((allStudies: Studie[]) => {
      this.studies = allStudies;
      this.getMovie();
    }, (error => {
    }));
  }

  onChangeActorSelected(actorSelected) {
    actorSelected = actorSelected.detail.value;
    this.addMovieActors(actorSelected.id);

    this.actors.forEach((actor, index) => {
      if (actor.id === actorSelected.id) {
        this.actorsForm.push(actorSelected);
      }
    });

  }

  addGenre() {
    this.addMovieGenres(this.genreFiel);
    this.genreForm.push(this.genreFiel);
    this.genreFiel = '';
  }

  removeGenreChip(genreSelected: string) {
    this.genreForm.forEach((genre, index) => {
      if (genre === genreSelected) {
        this.genreForm.splice(index, 1);
        this.removeMovieGenres(index);
      }
    });
  }

  removeActorChip(actorRemove) {
    this.actorsForm.forEach((actor, index) => {
      if (actor.id === actorRemove.id) {
        this.actorsForm.splice(index, 1);
        this.removeMovieActors(index);
        this.actors.push(actorRemove);
      }
    });
  }

  setActorsForm(actors) {
    this.actorsForm = this.actorService.getNameOfActors(actors, this.actors);
    actors.forEach(actor => {
      this.addMovieActors(actor);
    });
  }

  setGenreForm(allGenre){
    this.genreForm = allGenre;
    this.genreForm.forEach(genre => {
      this.addMovieGenres(genre);
    });
  }

  getTranslate() {
    this.translate.stream('EDITMOVIE').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }

}
