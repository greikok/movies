import { Injectable, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MoviesPageRoutingModule } from './movies-routing.module';

import { MoviesPage } from './movies.page';
import { Attributes, IntersectionObserverHooks, LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';
import { ComponentsModule } from '@components/components.module';
import { DEFAULTIMAGE, ERRORIMAGE, toastNotice } from '@utils/const';
import { ToastService } from '@services/toast.service';
import { switchMap } from 'rxjs/operators';
import { from } from 'rxjs';
import { DetailsMovieComponent } from './details-movie/details-movie.component';
import { CreateMovieComponent } from './create-movie/create-movie.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { TimePipe } from '@pipes/time.pipe';
import { EditMovieComponent } from './edit-movie/edit-movie.component';

@Injectable()

export class LazyLoadImageHooks extends IntersectionObserverHooks {
  poster: string;
  constructor(private toastService: ToastService,
    private translate: TranslateService) {
    super();

    this.translate.stream('POSTERLOADING').subscribe((response: string) => {
      this.poster = response;
    });

  };

  setup(attributes: Attributes) {
    attributes.offset = 10;
    attributes.defaultImagePath = DEFAULTIMAGE;
    attributes.errorImagePath = ERRORIMAGE;
    return super.setup(attributes);
  }

  loadImage(attributes: Attributes) {
    return from(this.toastService.toast(this.poster, toastNotice)).pipe(
      switchMap(_ => super.loadImage(attributes))
    );
  }

}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MoviesPageRoutingModule,
    LazyLoadImageModule,
    ComponentsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  providers: [{ provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks }],
  declarations: [
    MoviesPage,
    CreateMovieComponent,
    DetailsMovieComponent,
    EditMovieComponent,
    TimePipe
  ]
})
export class MoviesPageModule { }
