import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-studies',
  templateUrl: './studies.page.html',
  styleUrls: ['./studies.page.scss'],
})
export class StudiesPage implements OnInit {
  nameHeader: string;
  backHome = true;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.getTranslate();
  }

  getTranslate() {
    this.translate.stream('STUDIE').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }

}
