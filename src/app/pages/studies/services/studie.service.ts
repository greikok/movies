import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Studie } from '@interfaces/studie.interface';
import { ToastService } from '@services/toast.service';
import { toastError } from '@utils/const';
import { ToastMessages } from '@utils/enums';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudieService {
  apiUrl = environment.apiUrl;
  studie: Studie[];
  constructor(private http: HttpClient, private toastService: ToastService) { }

  getStudies(): Observable<Studie[]> {
    if (this.studie) {
      return of(this.studie);
    }
    return this.http.get<Studie[]>(`${this.apiUrl}/companies`)
      .pipe(
        tap(resp =>{
          this.studie = resp;
       }),
        catchError(error => {
          this.toastService.toast(ToastMessages.errorGettingMovies, toastError);
          throw new Error(error);
        }
        ));
  }
}
