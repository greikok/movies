import { TestBed } from '@angular/core/testing';

import { StudieService } from './studie.service';

describe('StudieService', () => {
  let service: StudieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
