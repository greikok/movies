import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(duration: number): string {
    if(Number.isInteger(duration)) {
      if(duration > 0)  {
        const hours = Math.floor( duration / 60);
        const minutes = duration % 60;
        return minutes > 0 ? `${hours}h ${minutes}m`: `${hours}h`;

      }else {
        return null;
      }
    } else {
      throw new Error('Number can not be converted');
    }
  }

}
