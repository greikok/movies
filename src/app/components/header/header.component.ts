import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() nameHeader: string;
  @Input() backHome: boolean;

  constructor(private navCtrl: NavController) { }

  ngOnInit() {}

  backMovies() {
    this.navCtrl.back();
  }

}
