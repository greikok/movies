import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PAGENOTFOUND } from '@utils/const';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {
  backHome = false;
  nameHeader: string;
  pageNotFound = PAGENOTFOUND;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.getTranslate();
  }

  getTranslate() {
    this.translate.stream('NOTFOUND').subscribe((response: string) => {
      this.nameHeader = response;
    });
  }


}
