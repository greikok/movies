import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MenuService } from '@services/menu.service';
import { Menu } from '@interfaces/menu';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  sideMenu: Observable<Menu[]>;
  langs: string[] = [];

  constructor(private menu: MenuController,
    private sideMenuService: MenuService,
    private translate: TranslateService) {
    this.langs = this.translate.getLangs();
  }

  ngOnInit() {
    this.sideMenu = this.sideMenuService.getMenuOpts();
  }

  closeMenu() {
    this.menu.close();
  }

  changeLang(event) {
    this.translate.use(event.detail.value);
  }

}
