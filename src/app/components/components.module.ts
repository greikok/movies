import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from './header/header.component';
import { NotFoundComponent } from './not-found/not-found.component';



@NgModule({
  declarations: [
    SideMenuComponent,
    HeaderComponent,
    NotFoundComponent,
  ],
  exports: [
    SideMenuComponent,
    HeaderComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    IonicModule,RouterModule,
    TranslateModule
  ]
})
export class ComponentsModule { }
